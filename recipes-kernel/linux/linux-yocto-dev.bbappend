FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

PR := "${PR}.1"

COMPATIBLE_MACHINE_nanopi-neo = "nanopi-neo"
KBRANCH_nanopi-neo  = "standard/base"
LINUX_VERSION_nanopi-neo ?= "4.13-rc+"
KMACHINE = "${MACHINE}"

KCONFIG_MODE="--alldefconfig"
KERNEL_EXTRA_ARGS += "LOADADDR=${UBOOT_ENTRYPOINT}"
KERNEL_EXTRA_FEATURES_nanopi-neo ?= ""

# Pull in the devicetree files into the rootfs
RDEPENDS_kernel-base += "kernel-devicetree"

SRC_URI += " file://nanopi-neo-standard.scc "
SRC_URI += " file://enable_i2c0.patch "

