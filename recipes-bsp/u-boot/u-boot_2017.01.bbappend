DESCRIPTION="Upstream's U-boot configured for sunxi devices"

COMPATIBLE_MACHINE = "nanopi-neo"
DEFAULT_PREFERENCE_sun8i="1"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
SRC_URI += "file://boot.cmd"
UBOOT_ENV_SUFFIX = "scr"
UBOOT_ENV = "boot"

do_configure_prepend() {
        echo "CONFIG_FS_EXT4=y" >> ${S}/configs/${UBOOT_MACHINE}
        echo "CONFIG_BOOTDELAY=0" >> ${S}/configs/${UBOOT_MACHINE}
}

do_compile_append() {
    ${B}/tools/mkimage -C none -A arm -T script -d ${WORKDIR}/boot.cmd ${WORKDIR}/${UBOOT_ENV_BINARY}
}

